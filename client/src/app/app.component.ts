import {Component} from '@angular/core';
import {UserService} from "./users/user.service";
import {OnInit} from "@angular/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private users: any = [];
  private selection = {
    _id: ""
  };
  private modalUI = {
    title: null
  };


  constructor(public userService: UserService) {

  }

  save(modal) {
    let user = this.selection;
    if (user._id) {
      this.userService.update(user)
        .subscribe((data) => {
          this.resetSelection();
          this.refresh();
          modal.hide();
        });
    }
    else {
      this.userService.create(user)
        .subscribe((data) => {
          this.resetSelection();
          this.users.push(data);
          modal.hide();
        });
    }
  }

  edit(user, modal) {
    this.selection = Object.assign({}, user);
    this.modalUI.title = 'Edit User';
    modal.show();
  }

  delete(user, modal) {
    this.selection = Object.assign({}, user);
    modal.show();
  }

  cancelDelete(modal) {
    this.resetSelection();
    modal.hide();
  }

  confirmDelete(modal) {
    this.userService.delete(this.selection)
      .subscribe(() => {
        this.refresh();
      }, () => {
      }, () => {
        this.resetSelection();
      });
    modal.hide();
  }


  new(modal) {
    this.resetSelection();
    this.modalUI.title = 'New User';
    modal.show();
  }

  refresh() {
    this.userService.findAll()
      .subscribe((users) => {
        this.users = users;
      });
  }

  ngOnInit() {
    this.refresh();
  }

  private resetSelection() {
    this.selection = {
      _id: ""
    };
  }

  private findUser(id) {
    return this.users.filter((user) => {
      return id === user._id;
    });
  }

}
