import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {environment} from "../../environments/environment";

@Injectable()
export class UserService {
  private usersUrl: "/users";

  constructor(public http: Http) {
  }

  findAll() {
    let url = this.constructUrl();
    return this.http.get(url)
      .map(this.extractData)
      .catch(this.handleError);
  }

  update(user: any) {
    let url = this.constructUrl(user._id);
    return this.http.put(url, user)
      .map(this.extractData)
      .catch(this.handleError);
  }

  create(user: any) {
    delete user._id;
    let url = this.constructUrl(this.usersUrl);
    return this.http.post(url, user)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delete(user) {
    let url = this.constructUrl(user._id);
    return this.http.delete(url, user)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private constructUrl(id?) {
    let result = `${environment.API_URL}/users`;
    if (id) {
      result = result + '/' + id;
    }
    return result;
  }

  private extractData(res: Response) {
    return res.json();
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
