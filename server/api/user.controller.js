'use strict';

const userService = require('../services/user.service');

module.exports.findAll = (request, response, next) => {
    userService.findAll()
        .then((users) => {
            response.send(users);
        })
        .catch(next);
};

module.exports.findOne = (request, response, next) => {

    let id = request.params.id;
    userService.findOne(id)
        .then((user) => {
            response.send(user);
        })
        .catch(next);

};

module.exports.create = (request, response, next) => {
    console.log('CREATE USER', request.body);
    let payload = request.body;
    userService.create(payload)
        .then((user) => {
            response.send(user);
        })
        .catch(next);
};

module.exports.update = (request, response, next) => {
    console.log('UPDATE USER', request.body);
    let id = request.params.id;
    let payload = request.body;
    userService.update(id, payload)
        .then((user) => {
            console.log('UPDATE USER:DONE', user);
            response.send(user);
        })
        .catch(next);
};

module.exports.delete = (request, response, next) => {
    let id = request.params.id;
    userService.delete(id)
        .then(() => {
            response.status(200).send({success: true});
        })
        .catch(next);
};