'use strict';

require('dotenv').config();

const fs = require('fs');
const join = require('path').join;
const express = require('express');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const config = require('./config');

const models = join(__dirname, 'models');
const port = process.env.PORT || 3000;

const app = express();
const connection = connect();

module.exports = {
    app,
    connection
};

// Bootstrap mongoose models
fs.readdirSync(models)
    .filter(file => ~file.indexOf('.js'))
    .forEach(file => require(join(models, file)));

// Bootstrap routes
require('./config/express')(app);
require('./config/routes')(app);

connection
    .on('error', console.log)
    .on('disconnected', connect)
    .once('open', listen);

function listen() {
    app.listen(port);
    console.log('Express app started on port ' + port);
}

function connect() {
    let options = {server: {socketOptions: {keepAlive: 1}}};
    return mongoose.connect(config.db, options).connection;
}
