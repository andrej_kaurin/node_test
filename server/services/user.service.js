'use strict';
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const User = mongoose.model('User');

module.exports.findAll = () => {
    return User.find({});
};

module.exports.findOne = (id) => {
    return User.findOne({_id: id});
};

module.exports.create = (payload) => {
    payload.hashed_password = bcrypt.hashSync(payload.password);
    let user = new User(payload);
    return user.save(user);
};

module.exports.update = (id, payload) => {
    if (payload.hashed_password) {
        payload.hashed_password = bcrypt.hashSync(payload.hashed_password);
    }
    return User.findOneAndUpdate({_id: id}, payload)
        .then(() => {
            return User.findOne({_id: id});
        });
};

module.exports.delete = (id) => {
    return User.remove({_id: id});
};