'use strict';

const user = require('../api/user.controller');


module.exports = function (app) {

    app.use('/api/users', require('../api/users.routes'));

    /**
     * Error handling
     */

    app.use(function (err, req, res, next) {
        // treat as 404
        if (err.message
            && (~err.message.indexOf('not found')
            || (~err.message.indexOf('Cast to ObjectId failed')))) {
            return next();
        }
        console.error(err.stack);
        // error page
        res.status(500).send({status: 500, error: err.stack});
    });

    // assume 404 since no middleware responded
    app.use(function (req, res, next) {
        res.status(404).send({
            status: 404,
            error: 'Not found'
        });
    });
};
