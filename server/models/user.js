'use strict';

const mongoose = require('mongoose');
const userPlugin = require('mongoose-user');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {type: String, default: ''},
    firstName: {type: String, default: ''},
    lastName: {type: String, default: ''},
    email: {type: String, default: ''},
    hashed_password: {type: String, default: ''},
    salt: {type: String, default: ''}
});

UserSchema.plugin(userPlugin, {});
mongoose.model('User', UserSchema);
