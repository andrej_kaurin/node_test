# Client
Client part was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3.
## Prerequisites
* [angular-cli](https://github.com/angular/angular-cli) (version 1.0.0-beta.28.3.)
## Installation
* "cd" to "client" folder
* run "npm install"
* Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
* Optionally you can change base API_URL in "environment" files ("client/src/environments")
# Server
## Prerequisites
* Make sure mongo is installed
* run mongo using "mongod" (or "sudo mongod")
* "cd" to "server" folder
* run "npm install"
* run "NODE_ENV=development node server.js"
